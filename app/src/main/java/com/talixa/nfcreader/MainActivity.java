package com.talixa.nfcreader;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;

import org.ndeftools.Message;
import org.ndeftools.Record;
import org.ndeftools.wellknown.TextRecord;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView txtId;
    private TextView txtContent;
    private CheckBox ckbWrite;

    private static final String TAG = "com.talixa.nfcreader";

    protected NfcAdapter nfcAdapter;
    protected PendingIntent nfcPendingIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get widget references
        txtId = (TextView) findViewById(R.id.txtId);
        txtContent = (TextView) findViewById(R.id.txtContent);
        ckbWrite = (CheckBox) findViewById(R.id.writeData);

        // initialize NFC
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (nfcAdapter.isEnabled()) {
            txtId.setText("Waiting...");
        } else {
            txtId.setText("NFC not enabled");
        }
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    public void enableForegroundMode() {
        Log.d(TAG, "enableForegroundMode");
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED); // filter for all
        IntentFilter[] writeTagFilters = new IntentFilter[]{tagDetected};
        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }

    public void disableForegroundMode() {
        Log.d(TAG, "disableForegroundMode");
        nfcAdapter.disableForegroundDispatch(this);
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 3];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ':';
        }

        return new String(hexChars).substring(0,hexChars.length-1);
    }

    @Override
    public void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent");

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            if (!ckbWrite.isChecked()) {
                readTag(intent);
            } else {
                writeTag(intent, System.currentTimeMillis());
            }
        }
    }

    private void readTag(Intent intent) {
        Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        Bundle extras = intent.getExtras();

        try {
            byte[] id = (byte[])extras.get(NfcAdapter.EXTRA_ID);
            txtId.setText(bytesToHex(id));

            Tag tag = (android.nfc.Tag)extras.get(NfcAdapter.EXTRA_TAG);

            for(int i = 0; i < tag.getTechList().length; ++i) {
                Log.d(TAG, tag.getTechList()[i]);
            }
            Log.d(TAG, tag.toString());
        } catch (Exception e) {
            Log.e(TAG, "Error reading NFC id");
        }

        if (messages != null) {
            Log.d(TAG, "Found " + messages.length + " NDEF messages"); // is almost always just one

            vibrate(); // signal found messages :-)

            // parse to records
            for (int i = 0; i < messages.length; i++) {
                try {
                    List<Record> records = new Message((NdefMessage) messages[i]);

                    Log.d(TAG, "Found " + records.size() + " records in message " + i);

                    for (int k = 0; k < records.size(); k++) {
                        Log.d(TAG, " Record #" + k + " is of class " + records.get(k).getClass().getSimpleName());

                        Record record = records.get(k);

                        if (record instanceof TextRecord) {
                            TextRecord tr = (TextRecord) record;
                            txtContent.setText(tr.getText());
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Problem parsing message", e);
                }
            }
        }
    }

    private void writeTag(Intent intent, long data) {
        Bundle extras = intent.getExtras();

        try {
            byte[] id = (byte[])extras.get(NfcAdapter.EXTRA_ID);
            txtId.setText(bytesToHex(id));

            Tag tag = (android.nfc.Tag)extras.get(NfcAdapter.EXTRA_TAG);
            Ndef ndef = Ndef.get(tag);

            ndef.connect();
            ndef.writeNdefMessage(new NdefMessage(createTextRecord(String.valueOf(data), Locale.ENGLISH, true)));
            txtContent.setText("!" + data + "!");
        } catch (Exception e) {
            Log.e(TAG, "Error writing NFC");
        }
    }

    public NdefRecord createTextRecord(String payload, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));
        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = payload.getBytes(utfEncoding);
        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);
        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);
        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableForegroundMode();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableForegroundMode();
    }

    private void vibrate() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(500);
    }
}